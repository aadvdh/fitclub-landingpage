module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: "#ee786c",
        "primary-dark": "#d66c61",
        darkgreen: "#0b6c75",
        darkyellow: "#febb5b",
        darkpurple: "#665c83"
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
};
